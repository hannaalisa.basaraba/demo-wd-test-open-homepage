import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.safari.SafariDriver;

public class HomePageTest{
    @Test
    public void testOnlinerOpen(){
        WebDriver driver=new SafariDriver();
        driver.get("https://www.onliner.by/");
        String onlinerLogoXpath="//*[@id=\"container\"]/div/div/header/div[3]/div/div[1]/a";
        By onlinerLogoBy=By.xpath(onlinerLogoXpath);
        WebElement onlinerLogoElement= driver.findElement(onlinerLogoBy);
        Assert.assertTrue(onlinerLogoElement.isDisplayed());
        driver.quit();
    }

    @Test
    public void testAmazonOpen(){
        WebDriver driver=new SafariDriver();
        driver.get("https://www.amazon.com/");
        String amazonLogoXpath="//*[@id=\"nav-logo-sprites\"]";
        By amazonLogoBy=By.xpath(amazonLogoXpath);
        WebElement amazonLogoElement=driver.findElement(amazonLogoBy);
        Assert.assertTrue(amazonLogoElement.isDisplayed());
        driver.quit();
    }

    @Test
    public void testTicketProOpen(){
        WebDriver driver=new SafariDriver();
        driver.get("https://www.ticketpro.by/");
        String ticketProLogoXpath="/html/body/div[2]/header/div/div[1]/div[1]/div/a/img";
        By ticketProLogoBy=By.xpath(ticketProLogoXpath);
        WebElement ticketProLogoElement= driver.findElement(ticketProLogoBy);
        Assert.assertTrue(ticketProLogoElement.isDisplayed());
        driver.quit();
    }

    @Test
    public void testAlatanTourOpen(){
        WebDriver driver=new SafariDriver();
        driver.get("https://alatantour.by/");
        String alatanTourLogoXpath="/html/body/header/div/div/div/div/a/img";
        By alatanTourLogoBy=By.xpath(alatanTourLogoXpath);
        WebElement alatanTourLogoElement= driver.findElement(alatanTourLogoBy);
        Assert.assertTrue(alatanTourLogoElement.isDisplayed());
        driver.quit();
    }

    @Test
    public void testOlxOpen(){
        WebDriver driver=new SafariDriver();
        driver.get("https://www.olx.pl/");
        String olxLogoXpath="//*[@id=\"headerLogo\"]";
        By olxLogoBy=By.xpath(olxLogoXpath);
        WebElement olxLogoElement= driver.findElement(olxLogoBy);
        Assert.assertTrue(olxLogoElement.isDisplayed());
        driver.quit();
    }

    @Test
    public void testTripadvisorOpen(){
        WebDriver driver=new SafariDriver();
        driver.get("https://www.tripadvisor.com/");
        String tripadvisorLogoXpath="//*[@id=\"lithium-root\"]/header/div/nav/h1/picture/img";
        By tripadvisorLogoBy=By.xpath(tripadvisorLogoXpath);
        WebElement tripadvisorLogoElement= driver.findElement(tripadvisorLogoBy);
        Assert.assertTrue(tripadvisorLogoElement.isDisplayed());
        driver.quit();
    }
}


